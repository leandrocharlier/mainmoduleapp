package com.example.mainmoduleapp;

import android.app.Activity;

import com.example.cuentasmodulee.CuentasNavigation;
import com.example.tarjetasmodule.TarjetaFragment;

public class CuentasTarjetasNavigation implements CuentasNavigation {

    @Override
    public void goToTarjetas(Activity activity, String cuenta) {
        activity.getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new TarjetaFragment())
                .commitAllowingStateLoss();
    }

}
