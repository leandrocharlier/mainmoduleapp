package com.example.mainmoduleapp;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.cuentasmodulee.CuentasFragment;
import com.example.tarjetasmodule.TarjetaFragment;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        final CuentasFragment cuentasFragment = new CuentasFragment(new CuentasTarjetasNavigation());
        final TarjetaFragment tarjetaFragment = new TarjetaFragment();

        replaceFragment(cuentasFragment);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_cuentas:
                        replaceFragment(cuentasFragment);
                        break;
                    case R.id.menu_tarjetas:
                        replaceFragment(tarjetaFragment);
                        break;
                }
            return false;
            }
        });
    }

    private void replaceFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commitAllowingStateLoss();
    }
}
